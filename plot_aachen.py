from datetime import datetime, timedelta
import numpy as np
from numpy import array as ar
import matplotlib.pyplot as plt
import pandas
from scipy import interpolate

data = open('aachen').read().splitlines()
data.pop(0)

date = []
total = []
stadt = []
healed = []
dead = []

for line in data[::-1]:
    if line:
        split = line.split(',')
        date.append(datetime.strptime(split[0], '%d/%m/%Y'))
        total.append(int(split[1]))
        stadt.append(int(split[2]))
        healed.append(int(split[3]) if split[3].strip().isdigit() else np.nan)
        dead.append(int(split[4]))

date_from_day0 = [(d-date[0]).days for d in date]

staedteregion_aachen_population = 555465.
stadt_aachen_population = 248960.

ftotal = interpolate.interp1d(date_from_day0, total, kind='linear')
fstadt = interpolate.interp1d(date_from_day0, stadt, kind='linear')
sieben_tage_inzidenz_staedteregion = [(ftotal(date_from_day0[j]) - ftotal(date_from_day0[j]-7))\
                                      /(staedteregion_aachen_population/1.0e5)
                                      if date_from_day0[j]>=7 else np.nan
                                      for j in range(len(date))]
sieben_tage_inzidenz_stadt = [(fstadt(date_from_day0[j]) - fstadt(date_from_day0[j]-7))\
                               /(stadt_aachen_population/1.0e5)
                               if date_from_day0[j]>=7 else np.nan
                               for j in range(len(date))]
# yeni vakalar
daily_new_cases = np.gradient(total) / np.gradient(date_from_day0)

active_cases = ar(total)-ar(healed)-ar(dead)

explanation = '' #'(StädteRegion Aachen)'

# Son N güne exp fit
num_days_for_fit = 30
total_fit = ar([x for j, x in enumerate(total) if date_from_day0[-1]-date_from_day0[j] < num_days_for_fit])
aktiv_fit = ar([x for j, x in enumerate(active_cases) if date_from_day0[-1]-date_from_day0[j] < num_days_for_fit])
date_from_day0_fit = ar([x for x in date_from_day0 if date_from_day0[-1]-x < num_days_for_fit])
date_fit = [x for j, x in enumerate(date) if date_from_day0[-1]-date_from_day0[j] < num_days_for_fit]
def calculate_exp_coef(date_from_day0, values):
    log = np.log(values)
    fit_val = np.polyfit(date_from_day0, log, deg=1)
    return fit_val
def exp_coef_to_doubling_time(coefs):
    return np.log(2.)/coefs[0]
fit_val = calculate_exp_coef(date_from_day0_fit, total_fit)
total_doubling_time = exp_coef_to_doubling_time(fit_val)
aktiv_doubling_time = exp_coef_to_doubling_time(calculate_exp_coef(date_from_day0_fit, aktiv_fit))

# Total infections so far
percent_infected = round(100. * total[-1] / staedteregion_aachen_population, 2)
percent_infected_stadt = round(100. * stadt[-1] / stadt_aachen_population, 2)
print(f'So far {percent_infected} of StaedteRegion and {percent_infected_stadt} Stadt Aachen had a confirmed Covid-19 infection')

# Infection projections


fig=plt.figure()
plt.semilogy(date_fit, total_fit, '-o', label='Total infections')
plt.semilogy(date_fit, aktiv_fit, '-o', label='Active infections')
plt.legend()
plt.xlabel('Date')


fig=plt.figure()
fig.suptitle("sars-cov-2 cases in Stadt and StädteRegion Aachen \n Data from www.staedteregion-aachen.de/corona", fontweight="bold")

for cnt, plotfun in enumerate([plt.semilogy, plt.plot]):

    #plt.figure()
    plt.subplot(1, 3, cnt+1)
    plotfun(date, total, '-', label=f'Total cases {explanation}')
    plotfun(date, healed, '-', label=f'Healed cases {explanation}')
    plotfun(date, dead, '-', label=f'Deaths {explanation}')
    plotfun(date, active_cases, '-', label=f'Active cases {explanation}', linewidth=3, markersize=8)
    plotfun(date, daily_new_cases, '-', label=f'Daily New cases {explanation}', linewidth=3, markersize=8)
    #plotfun(date, stadt, '-o', label='Total cases (Stadt Aachen)')
    if plotfun==plt.semilogy:
        plotfun(date, len(date)*[staedteregion_aachen_population], '--', label='Aachen total population')
        plt.legend(loc='upper center', fancybox=True, shadow=True, ncol=2, bbox_to_anchor=(1.0, -0.12))
        plt.title('Semilog plot', fontweight="bold")
    else:
        plt.title('Linear plot', fontweight="bold")
        text = f'{num_days_for_fit}-day exponential fit:\n' +\
               f'Doubling time for total cases is {round(total_doubling_time,1)} days.\n' +\
               f'Doubling time for active cases is {round(aktiv_doubling_time,1)} days.\n'
        plt.text(date[0], 0.85*max(total), text, fontweight="bold")
    plt.gcf().autofmt_xdate()

plt.subplot(1, 3, 3)
plt.plot(date, sieben_tage_inzidenz_staedteregion, label='Staedteregion Aachen')
plt.plot(date, sieben_tage_inzidenz_stadt, label='Stadt Aachen')
plt.plot(date, len(date)*[30], '--', label='30')
plt.plot(date, len(date)*[50], '--', label='50')
plt.title('Sieben-Tage-Inzidenz - pro 100,000', fontweight="bold")
plt.gcf().autofmt_xdate()
plt.legend()

plt.show()

