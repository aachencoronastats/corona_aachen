from datetime import datetime, timedelta
from numpy import mean, arange
import matplotlib.pyplot as plt

data = open('aachen_todesfaelle').read().splitlines()

deaths = {}
for line in data:
    if line:
        split = line.split(',')
        date = datetime.strptime(split[0], '%d/%m/%Y')
        deaths[date] = split[1].split(' ')
dates = list(deaths.keys())

summaries = [f'In Aachen between {dates[-1].date()} and {dates[0].date()}:']


all_deaths = []
all_dates = []
for date, daily_deaths in deaths.items():
    all_deaths += daily_deaths
    all_dates += len(daily_deaths) * [date]
deaths_male = [d for d in all_deaths if d[-1]=='M']
deaths_female = [d for d in all_deaths if d[-1]=='F']
total_deaths = len(all_deaths)
total_deaths_male = len(deaths_male)
percent_male = 100.*total_deaths_male/total_deaths
total_deaths_female = len(deaths_female)
percent_female = 100.*total_deaths_female/total_deaths
summaries += [f'There were {total_deaths} total deaths due to covid19']
summaries += [f'{total_deaths_male} (or {round(percent_male, 2)}%) of those were men and {total_deaths_female} (or {round(percent_female, 2)}%) of those were women']


ages = [int(d[:-1]) for d in all_deaths]
ages_male = [int(d[:-1]) for d in deaths_male]
ages_female = [int(d[:-1]) for d in deaths_female]
average_age = mean(ages)
average_age_male = mean(ages_male)
average_age_female = mean(ages_female)
youngest = min(ages)
summaries += [f'The average age of deaths were {round(average_age, 1)}.']
summaries += [f'({round(average_age_male, 1)} for men and {round(average_age_female, 1)} for women)']
summaries += [f'The youngest person to die was {youngest} years old.']

print('\n'.join(summaries))



zipped = list(zip(all_deaths, all_dates))
zipped_male = [(float(z[0][:-1]), z[1]) for z in zipped if z[0][-1]=='M']
zipped_female = [(float(z[0][:-1]), z[1]) for z in zipped if z[0][-1]=='F']
deaths_male, dates_male = zip(*zipped_male)
deaths_female, dates_female = zip(*zipped_female)

plt.figure()
plt.plot(deaths_male, dates_male, 'bo', label='Men')
plt.plot(deaths_female, dates_female, 'ro', label='Women')
plt.gcf().autofmt_xdate()
plt.legend()
plt.xlabel('Age of deceased', fontweight="bold")
plt.ylabel('Date of death report', fontweight="bold")
plt.text(5, datetime.strptime('15/01/2020', '%d/%m/%Y'), '\n'.join(summaries), fontweight="bold")
plt.xlim([0,100])
plt.show()

plt.figure()
bins = arange(0, 100, step=5)
plt.hist(ages, bins=bins)
plt.xlim([0,100])
plt.xlabel('Age', fontweight="bold")
plt.ylabel('Number of deaths', fontweight="bold")
plt.title(f'Number of deaths in Aachen due to Covid-19\nbetween {dates[-1].date()} and {dates[0].date()} ', fontweight="bold")
plt.xticks(bins)
plt.show()

